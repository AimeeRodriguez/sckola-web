/**
 * Evaluation Plan Module
 * @namespace Modules
 */
(function(){
    'use strict';

    angular
        .module('sckola.task', ['ui.router','ui.bootstrap'])
        .run(addStateToScope)
        .config(getRoutes);

    addStateToScope.$inject = ['$rootScope', '$state', '$stateParams'];
    function addStateToScope($rootScope, $state, $stateParams){
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
    };

    getRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];
    function getRoutes($stateProvider, $urlRouterProvider){

        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('taskCreate', {
                parent: 'index',
                url: '/createTask',
                views:{
                    'navbar@': {
                        templateUrl: 'modules/navbar/partials/navbar.html',
                        controller: 'NavBarCtrl',
                        controllerAs: 'vm'
                    },
                    'sidemenu@':{
                        templateUrl: 'modules/sidemenu/partials/sidemenu.html',
                        controller: 'SideMenuCtrl',
                        controllerAs: 'vm'
                    },
                    'notifications@': {
                        templateUrl: 'modules/notification/partials/notification.html',
                        controller: 'NotificationCtrl',
                        controllerAs: 'vm'
                    },
                    'content@':{
                        templateUrl: 'modules/task/partials/createTask.html',
                        controller: 'TaskCTRL',
                        controllerAs: 'vm'
                    },
                    'footer@': {
                        templateUrl: '',
                        controller: '',
                        controllerAs: ''
                    },
                    'news@': {
                        templateUrl: 'modules/news/partials/news.html',
                        controller: 'NewsCtrl',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('taskUpdate', {
                parent: 'index',
                url: '/updateTask',
                views:{
                    'navbar@': {
                        templateUrl: 'modules/navbar/partials/navbar.html',
                        controller: 'NavBarCtrl',
                        controllerAs: 'vm'
                    },
                    'sidemenu@':{
                        templateUrl: 'modules/sidemenu/partials/sidemenu.html',
                        controller: 'SideMenuCtrl',
                        controllerAs: 'vm'
                    },
                    'notifications@': {
                        templateUrl: 'modules/notification/partials/notification.html',
                        controller: 'NotificationCtrl',
                        controllerAs: 'vm'
                    },
                    'content@':{
                        templateUrl: 'modules/task/partials/editTask.html',
                        controller: 'TaskCTRL',
                        controllerAs: 'vm'
                    },
                    'footer@': {
                        templateUrl: '',
                        controller: '',
                        controllerAs: ''
                    },
                    'news@': {
                        templateUrl: 'modules/news/partials/news.html',
                        controller: 'NewsCtrl',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('taskEdit', {
                parent: 'index',
                url: '/editTask/:idTask',
                views:{
                    'navbar@': {
                        templateUrl: 'modules/navbar/partials/navbar.html',
                        controller: 'NavBarCtrl',
                        controllerAs: 'vm'
                    },
                    'sidemenu@':{
                        templateUrl: 'modules/sidemenu/partials/sidemenu.html',
                        controller: 'SideMenuCtrl',
                        controllerAs: 'vm'
                    },
                    'notifications@': {
                        templateUrl: 'modules/notification/partials/notification.html',
                        controller: 'NotificationCtrl',
                        controllerAs: 'vm'
                    },
                    'content@':{
                        templateUrl: 'modules/task/partials/editTask.html',
                        controller: 'TaskCTRL',
                        controllerAs: 'vm'
                    },
                    'footer@': {
                        templateUrl: '',
                        controller: '',
                        controllerAs: ''
                    },
                    'news@': {
                        templateUrl: 'modules/news/partials/news.html',
                        controller: 'NewsCtrl',
                        controllerAs: 'vm'
                    }
                }
            })
    };
})();