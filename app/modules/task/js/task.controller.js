(function(){
    'use strict';

    angular
        .module('sckola.task')
        .controller('TaskCTRL', TaskCTRL);

    TaskCTRL.$inject = ['authentication','$scope', '$rootScope', '$stateParams', '$state','ComunidadMaterias', '$timeout','$uibModal', 'EvaluationPlanDetail','TaskCreate', 'TaskGet'];
    function TaskCTRL(authentication, $scope, $rootScope, $stateParams, $state,ComunidadMaterias, $timeout, $uibModal, EvaluationPlanDetail, TaskCreate, TaskGet){
        var vm = this;
        vm.messageInitial = "Por favor seleccione una comunidad";
        vm.showError = false;
        vm.errorMessage = "¡Oops! Algo ha salido mal";
        vm.showMessageInitial = true;
        vm.messageHelp = "";
        vm.showSuccessMessage = false;
        vm.showHelp = false;
        vm.successMessage = "";
        vm.task = {"matterCommunitySectionList":[],"name":null,"completionDate":null,"creationDate":null,"adjunt":null,"evaluationId":null,"description":null};
        vm.user = authentication.getUser();
        vm.evaluationTrue = false;
        var permission = authentication.currentUser();
        vm.community = authentication.getCommunity();
        $rootScope.tokenLoging = authentication.isLoggedIn();


        if(vm.community==null){
            vm.showHelp = true;
            vm.helpMessage = "Debe seleccionar una comunidad. ¡Para seleccionar una comunidad haga clic aquí!";
        }


        /**
         * @name helpUser
         * @desc Depending of the user state, if it has a community or not, a mmater or not,
         *       the state of the view is changed to the requirement.
         * @memberOf Controllers.TaskController
         */
        vm.helpUser = function(){
            if(vm.community == null){
                $state.go("communities");
            }
            else if (vm.communityMatters.length === 0){
                $state.go("matterHome");
            }
        };

        vm.loadInitial = function(){
            vm.showMessageInitial = false;
            if (vm.community !== undefined && vm.community !== undefined) {
                ComunidadMaterias.get({communityId:vm.community.communityOrigin.id,roleId:1,userId:vm.user.id},
                    function(success){
                        vm.communityMatters = success.response;
                        if(vm.communityMatters.length === 0){
                            vm.showHelp = true;
                            vm.helpMessage= "No posees clases asociadas en esta comunidad. ¡Para asociar una clase haga click aquí!";
                        }
                        vm.showMessageInitial = false;
                        $timeout(function(){
                            vm.terminate = true;
                        }, 1000);
                    },
                    function(error){
                        vm.error = "Error cargado data de comunidad";
                        vm.showMessageInitial = true;
                        vm.showErrorMessage();
                        $timeout(function(){
                            vm.terminate = true;
                        }, 1000);
                    }
                );
                if($stateParams.idTask != null){
                TaskGet.get({idTask:$stateParams.idTask},
                function(success){
                    vm.task = success.response;
                    vm.matterCommunitySection = vm.task.matterCommunitySectionList[0];
                    for(var o = 0; o < vm.communityMatters.length; o++){
                        if(vm.communityMatters[o].id == vm.task.matterCommunitySectionList[0].id){
                            vm.matterCommunitySection = vm.communityMatters[o];
                        }
                    }
                    var formato = vm.task.completionDate.split("-");
                    vm.completionDate = new Date(formato[2], formato[1] - 1, formato[0]);
                    vm.selectClass(vm.matterCommunitySection);
                })
                }
            }else{
                vm.terminate = true;
                vm.showMessageInitial = true;
            }
        };


        vm.selectClass = function(item){
            vm.evaluations = null;
            vm.task.evaluacion = null;
            if(item.evaluationPlan != null){
                EvaluationPlanDetail.get({planId: item.evaluationPlan.id},
                    function(success){
                        vm.evaluations = success.response.evaluations;
                        if(vm.task.evaluationId != null){
                            for(var i = 0; i < vm.evaluations.length; i++){
                                if(vm.evaluations[i].id == vm.task.evaluationId){
                                    vm.evaluationTrue = true;
                                    vm.task.evaluacion = vm.evaluations[i];
                                }
                            }
                        }
                    },
                    function(error){
                    });
            }
        };

        vm.createTask = function(){
            vm.task.matterCommunitySectionList = [];
            vm.task.matterCommunitySectionList.push(vm.matterCommunitySection);
            var dates = new Date();
            vm.task.creationDate = dates.getDate() + "-"+ (dates.getMonth() + 1) +"-"+ dates.getFullYear();
            vm.task.completionDate = vm.completionDate.getDate() + "-"+ (vm.completionDate.getMonth() + 1) +"-"+ vm.completionDate.getFullYear();
            if(vm.task.evaluacion != null){
                vm.task.evaluationId = vm.task.evaluacion.id;
            }else{
                vm.task.evaluationId = null;
            }
            TaskCreate.save(vm.task,
                function (success) {
                    $state.go('matterHome');
                },function(error){
                    vm.mensajeError = "¡Oops! Algo ha salido mal.";
                    $scope.modalInstanceError = $uibModal.open({
                        templateUrl: 'modules/modals/partials/modal-error.html',
                        scope: $scope,
                        size: 'sm',
                        keyboard  : false
                    });
                    vm.showErrorMessage();
                });
        };

        vm.editTask = function(){
            vm.task.completionDate = vm.completionDate.getDate() + "-"+ (vm.completionDate.getMonth() + 1) +"-"+ vm.completionDate.getFullYear();
            if(vm.task.evaluacion != null){
                vm.task.evaluationId = vm.task.evaluacion.id;
            }else{
                vm.task.evaluationId = null;
            }
            TaskCreate.update(vm.task,
                function (success) {
                    $state.go('matterHome');
                },function(error){
                    vm.mensajeError = "¡Oops! Algo ha salido mal.";
                    $scope.modalInstanceError = $uibModal.open({
                        templateUrl: 'modules/modals/partials/modal-error.html',
                        scope: $scope,
                        size: 'sm',
                        keyboard  : false
                    });
                    vm.showErrorMessage();
                });
        };

        vm.addAdjunt = function(){
            $scope.modalInstanceSuccess = $uibModal.open({
                templateUrl: 'modules/task/partials/modal/taskAdjunt_modal.html',
                scope: $scope,
                size: 'sm',
                keyboard  : false
            });
        };

        //Valida si el usuario esa autenticado con los permisos para la vista
        if (permission === null || authentication.getUser() === null) {
            $state.go('root');
            console.log("no esta autenticado:" + permission + " JSON:" + permission!==undefined?JSON.stringify(permission):"null");
        }else{
            // carga inicial por defecto
            vm.loadInitial();
        }

        //escucha cuando exista cambio en la comunidad seleccionada
        $scope.$on('community', function (evt, community) {
            vm.community = community;
            vm.loadInitial();
        });

        vm.cancel = function () {
            $scope.modalInstance.dismiss('cancel');
        };

        vm.ok = function () {
            $scope.modalInstance.dismiss('cancel');
        };

        vm.go = function(){
            $state.go('teacherHome');
        };

        /**
         * @name showErrorMessage
         * @desc Shows error message to the user
         * @memberOf Controllers.StudentController
         */
        vm.showErrorMessage = function(){
            vm.showError = true;
            $timeout(function(){
                vm.showError = false;
            },2500);
        };


        vm.showSuccessNotification = function(message){
            vm.successMessage = message;
            vm.showSuccessMessage = true;
            $timeout(function(){
                vm.showSuccessMessage = false;
            },2500);
        };

        vm.validationFecha = function(){
            var date = new Date();
            if(vm.completionDate < date){
                vm.completionDate = null;
                vm.messageDate = "Fecha de tarea invalida";
                vm.showMessageDate = true;
                $timeout(function () {
                    vm.showMessageDate = false;
                }, 2500);
            }
        };

        vm.cerrar = function(){
            $scope.modalInstanceSuccess.dismiss('cancel');
            if(vm.create){
                vm.create = false;
                $state.go('students');
            }
        };

        vm.salir = function(){
            $scope.modalInstanceError.dismiss('cancel');
        }

    }
})();

