(function(){
    'use strict';

    angular
        .module('sckola.task')
        .factory('TaskCreate',taskCreate)
        .factory('TaskGet', taskGet);

    //Crear tarea
    taskCreate.$inject = ['$resource','$rootScope'];
    function taskCreate($resource,$rootScope){
        return $resource($rootScope.domainServiceUrl+'/skola/task',null,
            {save: {
            method: 'POST'
            /*headers: {
             Authorization: 'Bearer '+ authentication.getToken()
             }*/
            },
            get: {
            method: 'GET'
            } ,
            query: {
                method: 'GET',
                    isArray:true
            },
            update: {
                method: 'PUT'
            }});
    };

    taskGet.$inject = ['$resource','$rootScope'];
    function taskGet($resource,$rootScope){
        return $resource($rootScope.domainServiceUrl+'/skola/task/:idTask');
    };
})();
